<table cellpadding="0" cellspacing="0" border="0" class="display groceryCrudTable table table-striped" id="<?php echo uniqid(); ?>">
	<thead>
		<tr>
			<?php foreach($columns as $column){?>
				<th><?php echo $column->display_as; ?></th>
			<?php }?>
			<?php if(!$unset_delete || !$unset_edit || !$unset_read || !empty($actions)){?>
			<th class='actions'><?php echo $this->l('list_actions'); ?></th>
			<?php }?>
		</tr>
	</thead>
	<tbody>
		<?php foreach($list as $num_row => $row){ ?>
		<tr id='row-<?php echo $num_row?>'>
			<?php foreach($columns as $column){?>
				<td><?php echo $row->{$column->field_name}?></td>
			<?php }?>
			<?php if(!$unset_delete || !$unset_edit || !$unset_read || !empty($actions)){?>
			<td class='actions'>

				<?php if(!empty($row->action_urls)){
					foreach($row->action_urls as $action_unique_id => $action_url){
						$action = $actions[$action_unique_id]; ?>
						<a href="<?php echo $action_url; ?>" class="btn btn-xs bg-purple waves-effect" role="button">
							<i class="material-icons <?php echo $action_unique_id;?>"><?php echo $action->css_class; ?></i>
							<span class="hidden-xs"><?php echo $action->label?></span>
						</a>
					<?php }
				} ?>

				<?php if(!$unset_read){?>
					<a href="<?php echo $row->read_url?>" class="btn btn-xs bg-purple waves-effect" role="button">
						<i class="material-icons">description</i>
						<span class="hidden-xs"><?php echo $this->l('list_view');?></span>
					</a>
				<?php }?>

                <?php if(!$unset_clone){?>
                    <a href="<?php echo $row->clone_url?>" class="btn btn-xs bg-purple waves-effect" role="button">
						<i class="material-icons">content_copy</i>
                        <span class="hidden-xs"><?php echo $this->l('list_clone'); ?></span>
                    </a>
                <?php }?>

				<?php if(!$unset_edit){?>
					<a href="<?php echo $row->edit_url?>" class="btn btn-xs bg-purple waves-effect" role="button">
						<i class="material-icons">edit</i>
						<span class="hidden-xs"><?php echo $this->l('list_edit'); ?></span>
					</a>
				<?php }?>

				<?php if(!$unset_delete){?>
					<a onclick = "javascript: return delete_row('<?php echo $row->delete_url?>', '<?php echo $num_row?>')"
						href="javascript:void(0)" class="btn btn-xs bg-red waves-effect" role="button">
						<i class="material-icons">delete</i>
						<span class="hidden-xs"><?php echo $this->l('list_delete'); ?></span>
					</a>
				<?php }?>
			</td>
			<?php }?>
		</tr>
		<?php }?>
	</tbody>
</table>
