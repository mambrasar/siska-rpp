<?php defined('BASEPATH') || exit('No direct script access allowed'); ?>
<div class="panel panel-default">
	<div class="panel-heading">
        <h3 class="panel-title"><?php echo $title; ?></h3>
        <small><?php echo $section; ?></small>
  	</div>
    <?php echo form_open( $import_url, 'method="post" id="crudForm" enctype="multipart/form-data"'); ?>
    <div class="panel-body">
        <textarea name="imported_text" class="form-control" rows="10"></textarea>
    </div>
    <div class="panel-footer">
        <button type="submit" name="import" class="btn btn-success">Import</button>
    </div>
    <?php echo form_close() ?> 
</div>