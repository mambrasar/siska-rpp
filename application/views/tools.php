<?php defined('BASEPATH') || exit('No direct script access allowed'); ?>
<!-- Basic Table -->
<div class="spacer"></div>

<div class="block-header">
    <h2><?php echo $section; ?></h2>
</div>

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Kalender Pendidikan
                    <small>Tahun Ajaran <?php echo $year_start; ?> / <?php echo $year_end; ?></small>
                </h2>
            </div>
            <div class="body table-responsive">
                <table class="table table-bordered font-10">
                    <thead>
                        <tr>
                            <th class="align-center" rowspan="2">No</th>
                            <th class="align-center" rowspan="2">Bulan</th>
                            <th class="align-center" colspan="31">Tanggal</th>
                        </tr>
                        <tr>
                            <?php for($i=1;$i<=31;$i++): ?>
                            <th class="align-center"><?php echo $i; ?></th>
                            <?php endfor; ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(isset($events)): ?>
                        <?php $i=0; ?>
                        <?php foreach($events as $event): ?>
                        <?php $i++; ?>
                        <tr>
                            <th class="align-center" scope="row"><?php echo $i; ?></th>
                            <td><?php echo $event['name']; ?><?php echo $event['year']; ?></td>
                            <?php for($j=1;$j<=31;$j++): ?>
                                <?php if($event['days'][$j][0]==0): ?>
                                <td class="bg-grey"></td>
                                <?php elseif($event['days'][$j][0]==1): ?>
                                <td class="bg-grey align-center"><?php echo $event['days'][$j][1]; ?></td>
                                <?php elseif($event['days'][$j][0]==2): ?>
                                <td></td>
                                <?php elseif($event['days'][$j][0]==3): ?>
                                <td class="bg-red align-center"><?php echo $event['days'][$j][1]; ?></td>
                                <?php elseif($event['days'][$j][0]==4): ?>
                                <td class="bg-orange align-center"><?php echo $event['days'][$j][1]; ?></td>
                                <?php elseif($event['days'][$j][0]==5): ?>
                                <td class="bg-deep-purple align-center"><?php echo $event['days'][$j][1]; ?></td>
                                <?php elseif($event['days'][$j][0]==6): ?>
                                <td class="bg-pink align-center"><?php echo $event['days'][$j][1]; ?></td>
                                <?php elseif($event['days'][$j][0]==7): ?>
                                <td class="bg-amber align-center"><?php echo $event['days'][$j][1]; ?></td>
                                <?php elseif($event['days'][$j][0]==8): ?>
                                <td class="bg-purple align-center"><?php echo $event['days'][$j][1]; ?></td>
                                <?php else: ?>
                                <td></td>
                                <?php endif; ?>
                            <?php endfor; ?>
                        </tr>
                        <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
            <div class="header">
                <h2>
                    Alokasi Waktu
                    <small>Tahun Ajaran <?php echo $year_start; ?> / <?php echo $year_end; ?></small>
                </h2>
            </div>
            <div class="body table-responsive">
                <table class="table table-bordered font-10">
                    <thead>
                        <tr>
                            <th class="align-center" rowspan="2">No</th>
                            <th class="align-center" rowspan="2">Kompotensi Dasar</th>
                            <th class="align-center" rowspan="2">Minggu</th>
                            <?php foreach($events as $event): ?>
                            <th class="align-center" colspan="<?php echo $event['total_week']; ?>"><?php echo $event['name']; ?></br><?php echo $event['year']; ?></th>
                            <?php endforeach; ?>
                        </tr>
                        <tr>
                            <?php foreach($events as $event): ?>
                            <?php $i=0; ?>
                            <?php $weeks = $event['weeks']; ?>
                            <?php foreach($weeks as $week): ?>
                                <?php $i++; ?>
                                <th class="align-center" width="15"><?php echo $i; ?></th>
                            <?php endforeach; ?>
                            <?php endforeach; ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i=0; ?>
                        <?php $start=1; ?>
                        <?php $end=1; ?>
                        <?php foreach($competencies as $competency): ?>
                        <?php $i++; ?>
                        <tr>
                            <td rowspan="2" class="align-center"><?php echo $i; ?></td>
                            <td>3.<?php echo $i; ?>. <?php echo $competency['knowledges']; ?></td>
                            <td rowspan="2" class="align-center"><?php echo $competency['weeks']; ?></td>
                            <?php $counter=0; ?>
                            <?php $end = $start + $competency['weeks'] - 1; ?>
                            <?php foreach($events as $event): ?>
                            <?php foreach($event['weeks'] as $week): ?>
                                <?php if($week[0]==2): ?>
                                    <?php $counter++; ?>
                                    <?php if($counter >= $start AND $counter < $end): ?>
                                        <td rowspan="2" class="bg-blue align-center"></td>
                                    <?php elseif($counter >= $start AND $counter == $end): ?>
                                        <?php $start = $end + 1; ?>
                                        <td rowspan="2" class="bg-blue align-center"></td>
                                    <?php else: ?>
                                        <td rowspan="2" class="bg-white align-center"></td>
                                    <?php endif; ?>
                                <?php elseif($week[0]==3): ?>
                                    <td rowspan="2" class="bg-red align-center"><?php echo $week[1]; ?></td>
                                <?php elseif($week[0]==4): ?>
                                    <td rowspan="2" class="bg-orange align-center"><?php echo $week[1]; ?></td>
                                <?php elseif($week[0]==5): ?>
                                    <td rowspan="2" class="bg-deep-purple align-center"><?php echo $week[1]; ?></td>
                                <?php elseif($week[0]==6): ?>
                                    <td rowspan="2" class="bg-pink align-center"><?php echo $week[1]; ?></td>
                                <?php elseif($week[0]==7): ?>
                                    <td rowspan="2" class="bg-yellow align-center"><?php echo $week[1]; ?></td>
                                <?php elseif($week[0]==8): ?>
                                    <td rowspan="2" class="bg-purple align-center"><?php echo $week[1]; ?></td>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <?php endforeach; ?>
                        </tr>
                        <tr>
                            <td>4.<?php echo $i; ?>. <?php echo$competency['skills']; ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- #END# Basic Table -->