<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Material_items extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->library('aurora');
		$this->load->library('grocery_CRUD');
	}

	public function index($material_id=NULL,$parent_id=NULL)
	{
		if(is_null($material_id))
		{
			redirect('materials/index');
		}
		if(is_null($parent_id))
		{
			redirect(current_url().'/0');
		}
		$crud = new grocery_CRUD();
		$crud->set_table('material_items');
		$crud->set_relation('material_id','materials','title');
		$crud->where('material_items.material_id',$material_id); 
		$crud->where('material_items.parent_id',$parent_id);
		$crud->order_by('code','ASC');
		$crud->display_as('material_id','Material');
		$crud->display_as('code','Kode');
		$crud->display_as('title','Judul');
		$crud->display_as('content','Isi');
		$crud->display_as('activity','Aktifitas');
		$crud->display_as('judgement','Penilaian');
		$crud->field_type('parent_id','hidden');
		$crud->unset_columns(array('parent_id','content'));
		$crud->unset_texteditor('activity','judgement');
		$crud->add_action('Isi Materi', '', 'material_items/index/'.$material_id,'folder');
		$crud->unset_read();
		$crud->unset_clone();
		$data = $crud->render();
		$this->aurora->title = 'Isi Materi';
		$row = $this->db->where('material_id',$material_id)
						->get('materials')
						->row_array();
		$a_back = anchor('materials/index/'.$row['material_id'],$row['title']);
		$this->aurora->section = 'Materi : ' . $a_back;
		$this->aurora->publish($data);
	}

}