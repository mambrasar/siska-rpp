<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Curriculums extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->library('aurora');
		$this->load->library('grocery_CRUD');
	}

	public function index()
	{
		$this->aurora->title = 'Kurikulum';
		$crud = new grocery_CRUD();
		$crud->set_table('curriculums');
		$crud->display_as('name','Kurikulum');
		$crud->unset_columns(array('description'));
		$crud->add_action('Jenis Kurikulum', '', 'curriculum_categories/index','folder');
		$crud->unset_read();
		$crud->unset_clone();
		$data = $crud->render();
		$this->aurora->publish($data);
	}

}