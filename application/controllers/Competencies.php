<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Competencies extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->helper('html');
		$this->load->library('aurora');
		$this->load->library('grocery_CRUD');
	}

	public function index($subject_id=NULL)
	{
		if(is_null($subject_id))
		{
			redirect('expertises/index','refresh');
		}
		$crud = new grocery_CRUD();
		$crud->set_table('competencies');
		$crud->set_relation_n_n('materials', 'competency_materials', 'material_items', 'competency_id', 'material_item_id', 'title','priority',array('parent_id'=>0));
		$crud->set_relation('subject_id','subjects','name');
		$crud->where('competencies.subject_id',$subject_id); 
		$crud->order_by('code','ASC');
		$crud->display_as('code','Kode');
		$crud->display_as('knowledges','Pengetahuan');
		$crud->display_as('skills','Ketrampilan');
		$crud->display_as('weeks','Jumlah Minggu');
		$crud->display_as('alocation','Alokasi Waktu');
		$crud->display_as('subject_id','Mata Pelajaran');
		$crud->unset_columns(array('alocation','materials'));
		$crud->unset_texteditor('knowledges','skills');
		$crud->unset_read();
		$crud->unset_clone();
		$data = $crud->render();
		$this->aurora->title = 'Kompetensi Dasar';
		$row = $this->db->where('subject_id',$subject_id)
						->get('subjects')
						->row_array();
		$this->aurora->title = 'Mata Pelajaran : '.$row['name'];
		$a_back = anchor('subjects/index/'.$row['competency_skill_id'],'Mata Pelajaran');
		$this->aurora->section = $a_back . ' : '.$row['name'];
		$this->aurora->publish($data);
	}

	public function import_from_text($subject_id=NULL)
	{
		if(is_null($subject_id)) redirect('expertise_areas/index','refresh');
		$string = $this->input->post('imported_text');
		$data['result'] = '';
		if(!is_null($string))
		{
			$arrays = explode("\n",$string);
			$pos=4;
			$count=0;
			$array_new = array(); 
			foreach($arrays as $array)
			{
				if($pos==3)
				{
					$pos=4;
				}
				else 
				{
					$pos=3;
				}
				$array_new[$pos] = $array;
				if($pos==4)
				{
					$count++;
					foreach($array_new as $k => $v)
					{
						$array_new[$k] = trim(str_replace($k.'.'.$count,'',$v));
					}
					if($count<=9)
					{
						$count_string = '0'.$count;
					}
					else 
					{
						$count_string = ''.$count;
					}
					$arrays_new[$count_string] = $array_new;
					$array_new = array();
				}
			}
			foreach($arrays_new as $k => $v) 
			{
				$data = array( 
					'subject_id' => $subject_id,
					'code'=>$k,
					'knowledges' => $v[3],
					'skills' => $v[4],
					'weeks' => 0
				);
				$this->db->insert('competencies',$data);
			}
			redirect('competencies/index/'.$subject_id,'refresh');
		}
		$data['css_files'] = array();
		$data['js_files'] = array();
		$row = $this->db->where('subject_id',$subject_id)->get('subjects')->row_array();
		$data['title'] = 'Import';
		$data['section'] = 'Import Kompetensi Dasar dari Teks ke Mata Pelajaran '.$row['name'];
		$data['import_url'] = current_url();
		$data['output'] = $this->load->view('import_text',$data,TRUE);
		$this->aurora->publish($data);
	}

}