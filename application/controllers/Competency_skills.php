<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Competency_skills extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->helper('html');
		$this->load->library('aurora');
		$this->load->library('grocery_CRUD');
	}

	public function index($program_id=NULL)
	{
		if(is_null($program_id))
		{
			redirect('expertises/index','refresh');
		}
		$crud = new grocery_CRUD();
		$crud->set_table('competency_skills');
		$crud->set_relation('program_id','programs','name');
		$crud->where('competency_skills.program_id',$program_id); 
		$crud->order_by('code','ASC');
		$crud->display_as('code','Kode');
		$crud->display_as('name','Kompetensi Keahlian');
		$crud->display_as('program_id','Program Keahlian');
		$crud->add_action('Mata Pelajaran', '', 'subjects/index','folder');
		$crud->unset_read();
		$crud->unset_clone();
		$data = $crud->render();
		$this->aurora->title = 'Kompetensi Keahlian';
		$row = $this->db->where('program_id',$program_id)
						->get('programs')
						->row_array();
		$this->aurora->title = 'Program Keahlian : '.$row['name'];
		$a_back = anchor('programs/index/'.$row['expertise_id'],'Program Keahlian');
		$this->aurora->section = $a_back . ' : '.$row['name'];
		$this->aurora->publish($data);
	}

}