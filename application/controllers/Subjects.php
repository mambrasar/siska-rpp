<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Subjects extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->library('aurora');
		$this->load->library('grocery_CRUD');
	}

	public function index($competency_skill_id=NULL)
	{
		if(is_null($competency_skill_id))
		{
			redirect('expertises/index','refresh');
		}
		$crud = new grocery_CRUD();
		$crud->set_table('subjects');
        $crud->set_relation('competency_skill_id','competency_skills','name');
		$crud->set_relation('curriculum_category_id','curriculum_categories','name');
		$crud->set_relation('period_id','periods','name');
		$crud->where('subjects.competency_skill_id',$competency_skill_id); 
		$crud->display_as('code','Kode');
		$crud->display_as('name','Mata Pelajaran');
        $crud->display_as('competency_skill_id','Kompetensi Keahlian');
		$crud->display_as('curriculum_category_id','Jenis Kurikulum');
		$crud->add_action('KD', '', 'competencies/index','folder');
		$crud->add_action('Impor KD', '', 'competencies/import_from_text','folder');
		$crud->add_action('Perangkat', '', 'tools/index','folder');
		$crud->unset_read();
		$crud->unset_clone();
		$data = $crud->render();
		$this->aurora->title = 'Mata Pelajaran';
		$row = $this->db->where('competency_skill_id',$competency_skill_id)
						->get('competency_skills')
						->row_array();
		$this->aurora->title = 'Kompetensi Keahlian : '.$row['name'];
		$a_back = anchor('competency_skills/index/'.$row['program_id'],'Kompetensi Keahlian');
		$this->aurora->section = $a_back . ' : '.$row['name'];
		$this->aurora->publish($data);
	}

}