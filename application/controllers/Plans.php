<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Plans extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->library('aurora');
		$this->load->library('grocery_CRUD');
	}

	public function index($subject_id=NULL)
	{
		if(is_null($subject_id)) 
		{
			redirect('expertises/index','refresh');
		}
		$data=array();
		$this->aurora->publish($data,'print');
	}

}