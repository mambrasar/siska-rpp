<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Periods extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->library('aurora');
		$this->load->library('grocery_CRUD');
	}

	public function index()
	{
		$crud = new grocery_CRUD();
		$crud->set_table('periods');
		$crud->unset_read();
		$crud->unset_clone();
		$crud->display_as('name','Semester');
		$data = $crud->render();
		$this->aurora->title = 'Semester';
		$this->aurora->publish($data);
	}

}