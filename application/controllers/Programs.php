<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Programs extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->helper('html');
		$this->load->library('aurora');
		$this->load->library('grocery_CRUD');
	}

	public function index($expertise_id=NULL)
	{
		if(is_null($expertise_id))
		{
			redirect('expertise/index','refresh');
		}
		$crud = new grocery_CRUD();
		$crud->set_table('programs');
		$crud->set_relation('expertise_id','expertises','name');
		$crud->where('programs.expertise_id',$expertise_id);
		$crud->display_as('code','Kode');
		$crud->display_as('name','Program Keahlian');
		$crud->display_as('expertise_id','Bidang Keahlian');
		$crud->order_by('code','ASC'); 
		$crud->add_action('Kompetensi Keahlian', '', 'competency_skills/index','folder');
		$crud->unset_read();
		$crud->unset_clone();
		$data = $crud->render();
		$this->aurora->title = 'Program Keahlian';
		$row = $this->db->where('expertise_id',$expertise_id)
						->get('expertises')
						->row_array();
		$this->aurora->title = 'Bidang Keahlian : '.$row['name'];
		$a_back = anchor('expertises/index','Bidang Keahlian');
		$this->aurora->section = $a_back . ' : '.$row['name'];
		$this->aurora->publish($data);
	}

}