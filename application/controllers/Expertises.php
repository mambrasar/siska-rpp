<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Expertises extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->library('aurora');
		$this->load->library('grocery_CRUD');
	}

	public function index()
	{
		$crud = new grocery_CRUD();
		$crud->set_table('expertises');
		$crud->order_by('code','ASC');
		$crud->display_as('code','Kode');
		$crud->display_as('name','Bidang Keahlian');
		$crud->add_action('Program Keahlian', '', 'programs/index','folder');
		$crud->unset_read();
		$crud->unset_clone();
		$data = $crud->render();
		$this->aurora->title = 'Bidang Keahlian';
		$this->aurora->publish($data);
	}

}