<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tools extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
        $this->load->database();
		$this->load->helper('url');
		$this->load->helper('html');
        $this->load->library('aurora');
    }

    public function index($subject_id=NULL)
    {
        if(is_null($subject_id))
        {
            redirect('expertises/index','refresh');
        }
        redirect('tools/generate/'.$subject_id.'/2017','refresh');
    }

    public function generate($subject_id=NULL,$year_start='')
    {
        if(is_null($subject_id))
        {
            redirect('expertises/index','refresh');
        }        
        if($year_start=='')
        {
            redirect('expertises/index','refresh');
        }
        $data['subject'] = $this->db->where('subject_id',$subject_id)
                            ->get('subjects')
                            ->row_array();
        $data['competencies'] = $this->db->where('subject_id',$subject_id)
                            ->get('competencies')
                            ->result_array();
        $data['year_start'] = $year_start;
        $data['year_end'] = $year_start + 1;
        $data['period'] = $data['subject']['period_id'];
        $a_back_competency_skill = anchor('subjects/index/'.$data['subject']['competency_skill_id'],'Mata Pelajaran');
        $a_back_subject = anchor('competencies/index/'.$data['subject']['subject_id'],$data['subject']['name']);
        $data['section']= $a_back_competency_skill . ' : ' . $a_back_subject;
        $data = $this->_gen_calendar_per_year($data);
        $data = $this->_print_calendar($data);
        $this->aurora->publish($data,'print');
    }

    public function _print_calendar($data=NULL)
    {
		$data['css_files'] = array();
		$data['js_files'] = array();
        $data['output']=$this->load->view('tools',$data,TRUE);
        return $data;
    }

    public function _gen_calendar_per_year($data)
    {
        if(!isset($data['period']))
        {
            $data['period'] = 1;
        }
        if(!isset($data['year_start']))
        {
            $data['year_start'] = date('Y');
        } 
        if(!isset($data['year_end']))
        {
            $data['year_end'] = $data['year_start'] + 1;
        } 
        $this->load->config($data['year_start'].$data['year_end']);
        $data['calendar'] = $this->config->item('events');
        if($data['period']<>3)
        {
            $data['year'] = $data['year_start'];
            for($i=7;$i<=12;$i++)
            {
                $data['month'] = $i;
                $data = $this->_gen_calendar_per_period($data);
            }
            $week_mode = 0;
            $week_name = '';
            $day_of_week = 0;
            $active_day = 0;
            $active_week = 0;
            $passive_week = 0;
            $total_week_of_period = 0;
            for($i=7;$i<=12;$i++) 
            {
                $data['events'][$i]['total_week'] = 0;
                for($j=1;$j<=31;$j++)
                {
                    $day_mode = $data['events'][$i]['days'][$j][0];
                    $day_name = $data['events'][$i]['days'][$j][1];
                    if($day_mode>0)
                    {
                        if($day_name<>'')
                        {
                            $week_mode = $day_mode;
                            $week_name = $day_name;
                        }
                        $day_of_week++;
                        if($day_of_week==4)
                        {
                            $data['events'][$i]['total_week']++;
                            $week=array();
                            if($week_mode==1)
                            {
                                $week_mode=2;
                            }
                            $week[0]=$week_mode;
                            $week[1]=$week_name;
                            $data['events'][$i]['weeks'][]=$week;
                        }
                        if($day_mode==1)
                        {
                            $total_week_of_period++;
                            $week_mode = $day_mode;
                            $week_name = '';
                            $day_of_week = 0;
                            if($active_day>=6)
                            {
                                $active_week++;
                            }
                            else 
                            {
                                $passive_week++;
                            }
                            $active_day = 0;
                        }
                        elseif($day_mode==2)
                        {
                            $active_day++;
                        }
                    }
                }
            }
            $data['odd']['active_week'] = $active_week;
            $data['odd']['passive_week'] = $passive_week;
            $data['odd']['total_week'] = $total_week_of_period;
        }
        if($data['period']<>2)
        {
            $data['year'] = $data['year_end'];
            for($i=1;$i<=6;$i++)
            {
                $data['month'] = $i;
                $data = $this->_gen_calendar_per_period($data);
            }    
            $active_day = 0;
            $active_week = 0;
            $passive_week = 0;
            $total_week_of_period = 0;
            for($i=1;$i<=6;$i++) 
            {
                $data['events'][$i]['total_week'] = 0;
                for($j=1;$j<=31;$j++)
                {
                    $day_mode = $data['events'][$i]['days'][$j][0];
                    $day_name = $data['events'][$i]['days'][$j][1];
                    if($day_mode>0)
                    {
                        if($day_name<>'')
                        {
                            $week_mode = $day_mode;
                            $week_name = $day_name;
                        }
                        $day_of_week++;
                        if($day_of_week==4)
                        {
                            $data['events'][$i]['total_week']++;
                            if($week_mode==1)
                            {
                                $week_mode=2;
                            }
                            $week=array();
                            $week[0]=$week_mode;
                            $week[1]=$week_name;
                            $data['events'][$i]['weeks'][]=$week;
                        }
                        if($day_mode==1)
                        {
                            $total_week_of_period++;
                            $week_mode = $day_mode;
                            $week_name = '';
                            $day_of_week = 0;
                            if($active_day>=6)
                            {
                                $active_week++;
                            }
                            else 
                            {
                                $passive_week++;
                            }
                            $active_day = 0;
                        }
                        elseif($day_mode==2)
                        {
                            $active_day++;
                        }
                    }
                }
            }
            $data['even']['active_week'] = $active_week;
            $data['even']['passive_week'] = $passive_week;
            $data['even']['total_week'] = $total_week_of_period;        
        }
        unset($data['calendar']);
        return $data;
    }
    
    public function _gen_calendar_per_period($data)
    { 

        $month_names = array(
            'January' => 'Januari',
            'February' => 'Februari',
            'March' => 'Maret',
            'April' => 'April',
            'May' => 'Mei',
            'June' => 'Juni',
            'July' => 'Juli',
            'August' => 'Agustus',
            'September' => 'September',
            'October' => 'Oktober',
            'November' => 'November',
            'December' => 'Desember'
        );

        $month = $data['month'];
        $first_day_of_month = mktime(0,0,0,$data['month'],1,$data['year']);
        $date_components = getdate($first_day_of_month);
        $data['events'][$month]['name'] = $month_names[$date_components['month']];
        $data['events'][$month]['year'] = $data['year'];
        $day_of_week = $date_components['wday'];
        $days_on_the_month = date('t',$first_day_of_month);
        for($i=1;$i<=31;$i++)
        {
            if($day_of_week==7)
            {
                $day_of_week=0;
            }
            if($i<=$days_on_the_month)
            {
                if($day_of_week==0)
                {
                    $data['events'][$month]['days'][$i][0]=1;
                    $data['events'][$month]['days'][$i][1]='M';
                    $data['events'][$month]['days'][$i][2]='';   
                    $data['events'][$month]['days'][$i][3]=''; 
                }
                elseif(isset($data['calendar'][$month][$i]))
                {
                    $data['events'][$month]['days'][$i][0]=$data['calendar'][$month][$i][0];
                    $data['events'][$month]['days'][$i][1]=$data['calendar'][$month][$i][1];
                    $data['events'][$month]['days'][$i][2]=$data['calendar'][$month][$i][2]; 
                    if(isset($data['calendar'][$month][$i][3]))
                    {
                        $data['events'][$month]['days'][$i][3]=$data['calendar'][$month][$i][3];
                    }
                    else 
                    {
                        $data['events'][$month]['days'][$i][3]='';
                    }                      
                }
                else 
                {
                    $data['events'][$month]['days'][$i][0]=2;
                    $data['events'][$month]['days'][$i][1]='';
                    $data['events'][$month]['days'][$i][2]='';   
                    $data['events'][$month]['days'][$i][3]='';                     
                }
            }
            else 
            {
                $data['events'][$month]['days'][$i][0]=0;
                $data['events'][$month]['days'][$i][1]='';
                $data['events'][$month]['days'][$i][2]='';
                $data['events'][$month]['days'][$i][3]='';
            }
            $day_of_week++;
        }
        ksort($data['events'][$month]);
        unset($data['month']);
        return $data;     
    }

}