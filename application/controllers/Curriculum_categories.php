<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Curriculum_categories extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->library('aurora');
		$this->load->library('grocery_CRUD');
	}

	public function index($curriculum_id=NULL)
	{
		$crud = new grocery_CRUD();
		$crud->set_table('curriculum_categories');
		$crud->set_relation('curriculum_id','curriculums','name');
		$crud->where('curriculum_categories.curriculum_id',$curriculum_id); 
		$crud->display_as('curriculum_id','Kurikulum');
		$crud->display_as('name','Nama');
		$crud->display_as('spiritual','Spiritual');
		$crud->display_as('social','Sosial');
		$crud->unset_texteditor('spiritual','social');
		$crud->unset_columns(array('spiritual','social'));
		$crud->unset_read();
		$crud->unset_clone();
		$data = $crud->render();
		$this->aurora->title = 'Jenis Kurikulum';
		$row = $this->db->where('curriculum_id',$curriculum_id)
						->get('curriculums')
						->row_array();
		$this->aurora->title = 'Kurikulum : '.$row['name'];
		$this->aurora->publish($data);
	}

}