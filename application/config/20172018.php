<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * KETERANGAN
 * 0 = No Day
 * 1 = Sunday
 * 2 = Active
 * 3 = Libur Nasional
 * 4 = Libur Daerah Provinsi / Kabupaten
 * 5 = Libur Sekolah
 * 6 = Kegiatan Nasional
 * 7 = Kegiatan Daerah Provinsi / Kabupaten
 * 8 = Kegiatan Sekolah
 * .. 
 */

$config['events'] = array(
	7 => array(
        1 => array(5,'LS','pink'),
        2 => array(5,'LS','pink'),
        3 => array(5,'LS','pink'),
        4 => array(5,'LS','pink'),
        5 => array(5,'LS','pink'),
        6 => array(5,'LS','pink'),
        7 => array(5,'LS','pink'),
        8 => array(5,'LS','pink'),
        9 => array(5,'LS','pink'),
        10 => array(5,'LS','pink'),
        11 => array(5,'LS','pink'),
        12 => array(5,'LS','pink'),
        13 => array(5,'DUS','lime'),
        14 => array(5,'DUS','lime'),
        15 => array(5,'DUS','lime'),
        17 => array(5,'PLS','green'),
        18 => array(5,'PLS','green'),
        19 => array(5,'PLS','green'),
    ),
    8 => array(
        17 => array(3,'LU','blue','Proklamasi Kemerdekaan Republik Indonesia'),
    ),
    9 => array(
        1 => array(3,'LU','red','Hari Raya Idul Adha 1438H'),
        21 => array(3,'LU','red','Tahun Baru Hijriah 1439'),
    ),
    12 => array(
        4 => array(8,'UAS','yellow'),
        5 => array(8,'UAS','yellow'),
        6 => array(8,'UAS','yellow'),
        7 => array(8,'UAS','yellow'),
        8 => array(8,'UAS','yellow'),
        9 => array(8,'UAS','yellow'),
        11 => array(5,'KS','blue'),
        12 => array(5,'KS','blue'),
        13 => array(5,'KS','blue'),
        14 => array(5,'KS','blue'),
        15 => array(5,'KS','blue'),
        16 => array(5,'PR','blue'),
        18 => array(5,'LS','pink'),
        19 => array(5,'LS','pink'),
        20 => array(5,'LS','pink'),
        21 => array(5,'LS','pink'),
        22 => array(5,'LS','pink'),
        23 => array(5,'LS','pink'),
        25 => array(3,'LU','pink','Hari Raya Natal'),
        26 => array(5,'LS','pink'),
        27 => array(5,'LS','pink'),
        28 => array(5,'LS','pink'),
        29 => array(5,'LS','pink'),
        30 => array(5,'LS','pink'),
    ),
    1 => array(
        1 => array(5,'LS','pink'),
        2 => array(5,'LS','pink'),
        3 => array(5,'LS','pink'),
        4 => array(5,'LS','pink'),
        5 => array(5,'LS','pink'),
        6 => array(5,'LS','pink'),
    ),
    2 => array(
        5 => array(4,'LF','pink','HUT Injil Masuk Tanah Papua'),
    ),
    3 => array(
        18 => array(3,'LU','pink','Hari Raya Nyepi'),
        30 => array(3,'LU','pink','Jumaat Agung'),
    ),
    4 => array(
        2 => array(5,'UN','yellow'),
        3 => array(5,'UN','yellow'),
        4 => array(5,'UN','yellow'),
        5 => array(5,'UN','yellow'),
        9 => array(5,'UN','yellow'),
        10 => array(5,'UN','yellow'),
        11 => array(5,'UN','yellow'),
        12 => array(5,'UN','yellow'),
        13 => array(3,'LU','pink','Isra Miraj 1439 Hijrayah'),
        14 => array(3,'LF','pink'),
    ),
    5 => array(
        1 => array(3,'LU','pink','Hari Buruh'),
        10 => array(3,'LU','pink','Kenaikan Isa Almasih'),
    ),
    6 => array(
        4 => array(8,'UAS','yellow'),
        5 => array(8,'UAS','yellow'),
        6 => array(8,'UAS','yellow'),
        7 => array(8,'UAS','yellow'),
        8 => array(8,'UAS','yellow'),
        9 => array(8,'UAS','yellow'),
        12 => array(5,'PR','blue'),
        13 => array(5,'LS','pink'),
        14 => array(5,'LS','pink'),
        15 => array(3,'LU','pink','Hari Raya Idul Fitri'),
        16 => array(3,'LU','pink','Hari Raya Idul Fitri'),
        18 => array(5,'LS','pink'),
        19 => array(5,'LS','pink'),
        20 => array(5,'LS','pink'),
        21 => array(5,'LS','pink'),
        22 => array(5,'LS','pink'),
        23 => array(5,'LS','pink'),
        25 => array(5,'LS','pink'),
        26 => array(5,'LS','pink'),
        27 => array(5,'LS','pink'),
        28 => array(5,'LS','pink'),
        29 => array(5,'LS','pink'),
        30 => array(5,'LS','pink'),
    ),
);