<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Aurora
{

    protected $_ci;

    public $app = 'Perangkat Mengajar';
    public $title = '';
    public $section = '';
    public $page_title = '';

    public function __construct()
    {
        $this->_ci = &get_instance();
    }

    public function publish($data,$template='template')
    {
		if(!is_null($data))
		{
			if(is_object($data))
			{
				$data = (array) $data;
			}
        }
        $data['app'] = $this->app;
		if($this->title=='')
		{
            $data['page_title'] = $this->app;
            $data['title'] = '';
		}
		else 
		{
            $data['page_title'] = $this->app . ' - ' . $this->title;
            $data['title'] = $this->title;
        }
        if($this->section=='')
        { 
            if($this->title=='')
            {
                $data['section'] = '';
            }
            else 
            {
                $data['section'] = $this->title;
            }
        } 
        else 
        {
            $data['section'] = $this->section;
        }
		$this->_ci->load->view($template,$data);
	}

}